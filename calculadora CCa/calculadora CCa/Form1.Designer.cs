﻿namespace calculadora_CCa
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.rbtnsuma = new System.Windows.Forms.RadioButton();
            this.rbtndivision = new System.Windows.Forms.RadioButton();
            this.rbtnmultiplicacion = new System.Windows.Forms.RadioButton();
            this.rbtnresta = new System.Windows.Forms.RadioButton();
            this.txtoperador2 = new System.Windows.Forms.TextBox();
            this.txtoperador1 = new System.Windows.Forms.TextBox();
            this.txtresultado = new System.Windows.Forms.TextBox();
            this.btncalcular = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // rbtnsuma
            // 
            this.rbtnsuma.Appearance = System.Windows.Forms.Appearance.Button;
            this.rbtnsuma.BackColor = System.Drawing.Color.Chocolate;
            this.rbtnsuma.FlatAppearance.BorderSize = 2;
            this.rbtnsuma.FlatAppearance.CheckedBackColor = System.Drawing.Color.SaddleBrown;
            this.rbtnsuma.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Sienna;
            this.rbtnsuma.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Peru;
            this.rbtnsuma.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.rbtnsuma.Font = new System.Drawing.Font("Stencil", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.rbtnsuma.Location = new System.Drawing.Point(118, 54);
            this.rbtnsuma.Name = "rbtnsuma";
            this.rbtnsuma.Size = new System.Drawing.Size(30, 30);
            this.rbtnsuma.TabIndex = 0;
            this.rbtnsuma.TabStop = true;
            this.rbtnsuma.Text = "+";
            this.rbtnsuma.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.rbtnsuma.UseVisualStyleBackColor = false;
            // 
            // rbtndivision
            // 
            this.rbtndivision.Appearance = System.Windows.Forms.Appearance.Button;
            this.rbtndivision.BackColor = System.Drawing.Color.Chocolate;
            this.rbtndivision.FlatAppearance.BorderSize = 2;
            this.rbtndivision.FlatAppearance.CheckedBackColor = System.Drawing.Color.SaddleBrown;
            this.rbtndivision.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Sienna;
            this.rbtndivision.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Peru;
            this.rbtndivision.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.rbtndivision.Font = new System.Drawing.Font("Stencil", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.rbtndivision.Location = new System.Drawing.Point(164, 90);
            this.rbtndivision.Name = "rbtndivision";
            this.rbtndivision.Size = new System.Drawing.Size(30, 30);
            this.rbtndivision.TabIndex = 1;
            this.rbtndivision.TabStop = true;
            this.rbtndivision.Text = "÷";
            this.rbtndivision.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.rbtndivision.UseVisualStyleBackColor = false;
            // 
            // rbtnmultiplicacion
            // 
            this.rbtnmultiplicacion.Appearance = System.Windows.Forms.Appearance.Button;
            this.rbtnmultiplicacion.BackColor = System.Drawing.Color.Chocolate;
            this.rbtnmultiplicacion.FlatAppearance.BorderSize = 2;
            this.rbtnmultiplicacion.FlatAppearance.CheckedBackColor = System.Drawing.Color.SaddleBrown;
            this.rbtnmultiplicacion.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Sienna;
            this.rbtnmultiplicacion.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Peru;
            this.rbtnmultiplicacion.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.rbtnmultiplicacion.Font = new System.Drawing.Font("Stencil", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.rbtnmultiplicacion.Location = new System.Drawing.Point(118, 90);
            this.rbtnmultiplicacion.Name = "rbtnmultiplicacion";
            this.rbtnmultiplicacion.Size = new System.Drawing.Size(30, 30);
            this.rbtnmultiplicacion.TabIndex = 2;
            this.rbtnmultiplicacion.TabStop = true;
            this.rbtnmultiplicacion.Text = "×";
            this.rbtnmultiplicacion.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.rbtnmultiplicacion.UseVisualStyleBackColor = false;
            // 
            // rbtnresta
            // 
            this.rbtnresta.Appearance = System.Windows.Forms.Appearance.Button;
            this.rbtnresta.BackColor = System.Drawing.Color.Chocolate;
            this.rbtnresta.FlatAppearance.BorderSize = 2;
            this.rbtnresta.FlatAppearance.CheckedBackColor = System.Drawing.Color.SaddleBrown;
            this.rbtnresta.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Sienna;
            this.rbtnresta.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Peru;
            this.rbtnresta.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.rbtnresta.Font = new System.Drawing.Font("Stencil", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.rbtnresta.Location = new System.Drawing.Point(164, 54);
            this.rbtnresta.Name = "rbtnresta";
            this.rbtnresta.Size = new System.Drawing.Size(30, 30);
            this.rbtnresta.TabIndex = 3;
            this.rbtnresta.TabStop = true;
            this.rbtnresta.Text = "−";
            this.rbtnresta.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.rbtnresta.UseVisualStyleBackColor = false;
            // 
            // txtoperador2
            // 
            this.txtoperador2.Font = new System.Drawing.Font("Stencil", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.txtoperador2.ForeColor = System.Drawing.SystemColors.InfoText;
            this.txtoperador2.Location = new System.Drawing.Point(228, 74);
            this.txtoperador2.Name = "txtoperador2";
            this.txtoperador2.PlaceholderText = "0";
            this.txtoperador2.Size = new System.Drawing.Size(100, 26);
            this.txtoperador2.TabIndex = 5;
            this.txtoperador2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtoperador2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtoperador2_KeyPress);
            // 
            // txtoperador1
            // 
            this.txtoperador1.Font = new System.Drawing.Font("Stencil", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.txtoperador1.ForeColor = System.Drawing.SystemColors.InfoText;
            this.txtoperador1.Location = new System.Drawing.Point(12, 74);
            this.txtoperador1.Name = "txtoperador1";
            this.txtoperador1.PlaceholderText = "0";
            this.txtoperador1.Size = new System.Drawing.Size(100, 26);
            this.txtoperador1.TabIndex = 6;
            this.txtoperador1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtoperador1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtoperador1_KeyPress);
            // 
            // txtresultado
            // 
            this.txtresultado.Font = new System.Drawing.Font("Stencil", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.txtresultado.ForeColor = System.Drawing.SystemColors.InfoText;
            this.txtresultado.Location = new System.Drawing.Point(409, 74);
            this.txtresultado.Name = "txtresultado";
            this.txtresultado.PlaceholderText = "0";
            this.txtresultado.ReadOnly = true;
            this.txtresultado.Size = new System.Drawing.Size(100, 26);
            this.txtresultado.TabIndex = 7;
            this.txtresultado.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // btncalcular
            // 
            this.btncalcular.BackColor = System.Drawing.Color.Chocolate;
            this.btncalcular.FlatAppearance.BorderSize = 2;
            this.btncalcular.FlatAppearance.CheckedBackColor = System.Drawing.Color.SaddleBrown;
            this.btncalcular.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Sienna;
            this.btncalcular.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Peru;
            this.btncalcular.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btncalcular.Font = new System.Drawing.Font("Stencil", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.btncalcular.Location = new System.Drawing.Point(348, 74);
            this.btncalcular.Name = "btncalcular";
            this.btncalcular.Size = new System.Drawing.Size(30, 30);
            this.btncalcular.TabIndex = 8;
            this.btncalcular.Text = "=";
            this.btncalcular.UseVisualStyleBackColor = false;
            this.btncalcular.Click += new System.EventHandler(this.btncalcular_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::calculadora_CCa.Properties.Resources.fe1680d9e81708fd79fc27b791401673_icono_de_calculadora_plana;
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(100, 50);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 9;
            this.pictureBox1.TabStop = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DarkOrange;
            this.ClientSize = new System.Drawing.Size(560, 168);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.btncalcular);
            this.Controls.Add(this.txtresultado);
            this.Controls.Add(this.txtoperador1);
            this.Controls.Add(this.txtoperador2);
            this.Controls.Add(this.rbtnresta);
            this.Controls.Add(this.rbtnmultiplicacion);
            this.Controls.Add(this.rbtndivision);
            this.Controls.Add(this.rbtnsuma);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximumSize = new System.Drawing.Size(576, 207);
            this.MinimumSize = new System.Drawing.Size(576, 207);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "CALCULADORA";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private RadioButton rbtnsuma;
        private RadioButton rbtndivision;
        private RadioButton rbtnmultiplicacion;
        private RadioButton rbtnresta;
        private TextBox txtoperador2;
        private TextBox txtoperador1;
        private TextBox txtresultado;
        private Button btncalcular;
        private PictureBox pictureBox1;
    }
}