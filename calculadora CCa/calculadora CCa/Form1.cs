namespace calculadora_CCa
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void btncalcular_Click(object sender, EventArgs e)
        {
            double operando1 = Convert.ToDouble(txtoperador1.Text);
            double operando2 = Convert.ToDouble(txtoperador2.Text);
            double resultado = 0;

            if (rbtnsuma.Checked)
            {
                resultado =operando1 + operando2;
            }
            if (rbtnresta.Checked)
            {
                resultado = operando1 - operando2;
            }
            if (rbtnmultiplicacion.Checked)
            {
                resultado = operando1 * operando2;
            }
            if (rbtndivision.Checked)
            {
                resultado = operando1 / operando2;
            }

            txtresultado.Text=resultado.ToString();

        }

        private void txtoperador1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) &&
        (e.KeyChar != '.'))
            {
                e.Handled = true;
            }

            // only allow one decimal point
            if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
            }
        }

        private void txtoperador2_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) &&
        (e.KeyChar != '.'))
            {
                e.Handled = true;
            }

            // only allow one decimal point
            if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
            }
        }
    }
}